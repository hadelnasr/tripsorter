# Trip Sorter
---------------------
This is project is small library that provides a way to sort boarding cards and format the information about the trip
in a easy and understandable format.

## Requirements

 - PHP >= 5.5 (because of the `::class` constant in the test, the rest of the code base should work fine in php5.3)
 - PHPUnit 5.7.9, (I can't garanty it will work in a newer or older version.)
 - Set up your own autoloading or use Composer. it'll work fine in both case. (In case you use composer, there is a composer.json in the project)
 
## Configuration

No configuration is needed.

## Usage

the usage of the library is quite straight forward. Check this code snippet:

```php

use TripSorter\Formatter\TextFormatter;
use TripSorter\Sorter\BoardingCardSorter;
use TripSorter\BoardingCard\GenericBoardingCard;

//set up formatter with the sorter you want
$formatter = new TextFormatter();
$formatter->setSorter(new BoardingCardSorter());

//Create BoardingCard and the trip
$string = file_get_contents("data/trips.json");
$jsonTrips = json_decode($string, true);
$trips = [];
foreach ($jsonTrips as $jsonTrip){
    $boardingCardOrigin = new GenericBoardingCard();
    $boardingCardOrigin
        ->setOrigin($jsonTrip['origin'])
        ->setDestination($jsonTrip['destination'])
        ->setTransport($jsonTrip['transport'])
        ->setMean($jsonTrip['mean'])
        ->setSeat($jsonTrip['seat'])
        ->setOtherInformation($jsonTrip['others']);
    
    $trips[] = $boardingCardOrigin;
}

//Pass the trips to formatter

$text = $formatter->format($trips);
echo $text;

```
the variable `$text` should contain the trips orders

### Extends and customize the behavior

The code offer interface for most of the "responsability" that you can implement differently and still keep 
the overall system work fine. Here are the provided Interface
- [`TripSorter\BoardingCard\OrderableBoardingCardInterface`] : A class implementing this interface can be sorted by the [`TripSorter\Sorter\BoardingCardSorterInterface`]
- [`TripSorter\BoardingCard\BoardingCardInterface`] : A class implementing this interface can be formatted by the `TripSorter\Formatter\FormatterInterface`'s subclasses. It's extends [`TripSorter\BoardingCard\OrderableBoardingCardInterface`]
- [`TripSorter\Formatter\FormatterInterface`]: defines a simple interface for a formatter.
- [`TripSorter\Sorter\BoardingCardSorterInterface`]: defines a single method (`sort`) for a sorter.

### Unit Test

The unit test are situated in the tests folder. Here is how you run the test (I assume that you have phpunit installed globally)
`phpunit -c .`

To test your code just run 'phpunit' from your terminal to get the result

## Troubleshooting

In case the library doesn't work, make sure you have PHP5.5 or newer installed.
 ## FAQ
 
 ### How does the sorting algorithm performs?
  It has a optimistic complexity of O(n) in 3 cases:
  - When the cards are already ordered.
  - When they are ordered backward.
  - and in this case: take a list of 10 element 1 to 10, order them like this 5-4-6-3-7-2-8-1-9-10 (or any ordering that derive from this logique)
  It has a pessimistic complexity of O(n LOG n) (n `while` iterations and the iterations of the `for` loop being decremented by 1 in the worst case)
  