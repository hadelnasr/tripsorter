<?php

require(__DIR__ . '/vendor/autoload.php');

use TripSorter\Formatter\TextFormatter;
use TripSorter\Sorter\BoardingCardSorter;
use TripSorter\BoardingCard\GenericBoardingCard;

//set up formatter with the sorter you want
$formatter = new TextFormatter();
$formatter->setSorter(new BoardingCardSorter());

//Create BoardingCard and the trip
$jsonTrips = json_decode(file_get_contents("data/trips.json"), true);
$trips = [];
foreach ($jsonTrips as $jsonTrip){
    $boardingCardOrigin = new GenericBoardingCard();
    $boardingCardOrigin
        ->setOrigin($jsonTrip['origin'])
        ->setDestination($jsonTrip['destination'])
        ->setTransport($jsonTrip['transport'])
        ->setMean($jsonTrip['mean'])
        ->setSeat($jsonTrip['seat'])
        ->setOtherInformation($jsonTrip['others']);
    
    $trips[] = $boardingCardOrigin;
}

//Pass the trips to formatter

$text = $formatter->format($trips);
echo $text;