<?php

namespace TripSorter\Formatter;


use TripSorter\Sorter\BoardingCardSorterInterface;

interface FormatterInterface
{
    /**
     * Returns the formated version of the trip provided
     * @param array $trip
     * @throws \InvalidArgumentException if the sorter is not specified
     * @throws \InvalidArgumentException if the trip array is empty
     * @return mixed
     */
    public function format(array $trip);

    /**
     * @param BoardingCardSorterInterface $boardingCardSorter
     * @return mixed
     */
    public function setSorter(BoardingCardSorterInterface $boardingCardSorter);
}
