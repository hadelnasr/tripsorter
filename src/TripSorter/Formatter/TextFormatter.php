<?php

namespace TripSorter\Formatter;

use TripSorter\BoardingCard\BoardingCardInterface;
use TripSorter\Sorter\BoardingCardSorterInterface;

/**
 * Class TextFormatter
 * @package TripSorter\Formatter
 */
class TextFormatter implements FormatterInterface
{

    /**
     * @var BoardingCardSorterInterface
     */
    protected $sorter;

    /**
     * @inheritDoc
     */
    public function format(array $trip)
    {
        if (count($trip) == 0) {
            throw new \InvalidArgumentException("The trip is empty");
        }
        if (!$this->sorter instanceof BoardingCardSorterInterface) {
            throw new \InvalidArgumentException("Provide a sorter first");
        }

        $trip = $this->sorter->sort($trip);
        $text = "";
        $count = 1;
        while ($trip->valid()) {
            $text .= ($trip->key() + 1) . ". ";
            $text .= $this->formatItem($trip->current());
            $trip->next();
            $count++;
            $text .= "<hr/>";
        }

        return $text . $count . ". Congrats, you reached your final destination.";
    }

    /**
     * Format one boarding card object into human understandable text
     * @param BoardingCardInterface $boardingCard
     * @return string
     */
    protected function formatItem(BoardingCardInterface $boardingCard)
    {
        $text = sprintf(
            "From %s, Take the %s %s to %s.", $boardingCard->getOrigin(), $boardingCard->getTransport(), $boardingCard->getMean(), $boardingCard->getDestination()
        );

        if (!is_null($boardingCard->getSeat()) && strlen($boardingCard->getSeat()) > 0) {
            $text .= " Your seat is " . $boardingCard->getSeat() . ".";
        } else {
            $text .= " No particular sit is assigned for you.";
        }

        if (!is_null($boardingCard->getOtherInformation()) && strlen($boardingCard->getOtherInformation()) > 0) {
            $text .= " " . $boardingCard->getOtherInformation() . ".";
        }

        return $text . "\n";
    }

    /**
     * @inheritDoc
     */
    public function setSorter(BoardingCardSorterInterface $boardingCardSorter)
    {
        $this->sorter = $boardingCardSorter;
    }

}
