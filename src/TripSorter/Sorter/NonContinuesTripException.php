<?php

namespace TripSorter\Sorter;

/**
 * Class NonContinuesTripException
 * @package TripSorter\Sorter
 */
class NonContinuesTripException extends \Exception
{

}
