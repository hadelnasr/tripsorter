<?php

namespace TripSorter\Sorter;

/**
 * Interface BoardingCardSorterInterface
 * @package TripSorter\Sorter
 */
interface BoardingCardSorterInterface
{
    /**
     * Orders an array of unordered OrderableBoardingCardInterface object
     *
     * @param array $boardingCards
     * @return \Iterator
     * @throws NonContinuesTripException if the trip is not continues
     * @throws \InvalidArgumentException if an item that doesn't implement the interface OrderableBoardingCardInterface
     */
    public function sort(array $boardingCards);
}
