<?php

namespace TripSorter\BoardingCard;

/**
 * Interface OrderableBoardingCardInterface
 *
 * This interface defines the minimum requirement
 * for a boarding card that can be ordered based on
 * it's origin and departure
 *
 * @package TripSorter\BoardingCard
 */
interface OrderableBoardingCardInterface
{
    /**
     * The designation of the origin
     *
     * make sure to use the exact value for the card
     * that share the same origin (or departure)
     * @return string
     */
    public function getOrigin();

    /**
     * The designation of the origin
     *
     * make sure to use the exact value for the card
     * that share the same origin (or departure)
     * @return string
     */
    public function getDestination();

}
