<?php

namespace TripSorter\BoardingCard;

/**
 * Interface BoardingCardInterface
 *
 * This interface
 * @package TripSorter\BoardingCard
 */
interface BoardingCardInterface extends OrderableBoardingCardInterface
{
    /**
     * Returns the designation of the seat
     *
     * @return string|null
     */
    public function getSeat();

    /**
     * Returns the designation of the transport, e.g: Train, Plane
     * @return string
     */
    public function getTransport();

    /**
     * Returns the designation of the mean of transport,
     * in other word, the bus number, plane number,
     * the licence number of the uber car
     * @return string
     */
    public function getMean();

    /**
     * Any other information that is useful to know
     * @return string
     */
    public function getOtherInformation();

}
