<?php

use TripSorter\Formatter\TextFormatter;
use TripSorter\BoardingCard\GenericBoardingCard;
use TripSorter\Sorter\BoardingCardSorter;

class TextFormatterTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var array
     */
    protected $trip;

    /**
     * @var string
     */
    protected $text;

    public function setUp()
    {
        $this->trip = [];

        $boardingCardOrigin = new GenericBoardingCard();
        $boardingCardOrigin->setDestination('Saudai Arabia')
                ->setMean('B790')
                ->setOrigin('Egypt')
                ->setOtherInformation("Take money with you")
                ->setSeat('G12')
                ->setTransport('Plane');

        $boardingCardDestination = new GenericBoardingCard();
        $boardingCardDestination->setDestination('Algeria')
                ->setMean('GF88')
                ->setOrigin('Moroco')
                ->setOtherInformation(null)
                ->setSeat('AB22')
                ->setTransport('Train');

        $boardingCardStopover = new GenericBoardingCard();
        $boardingCardStopover->setTransport('Plane')
                ->setSeat("F33")
                ->setOtherInformation("IT's a good flight, enjoy it")
                ->setOrigin("Saudai Arabia")
                ->setMean("A750")
                ->setDestination("Moroco");


        $this->trip[] = $boardingCardStopover;
        $this->trip[] = $boardingCardDestination;
        $this->trip[] = $boardingCardOrigin;

        $this->text = "1. From Egypt, Take the Plane B790 to Saudai Arabia. Your seat is G12. Take money with you.\n<hr/>";
        $this->text .= "2. From Saudai Arabia, Take the Plane A750 to Moroco. Your seat is F33. IT's a good flight, enjoy it.\n<hr/>";
        $this->text .= "3. From Moroco, Take the Train GF88 to Algeria. Your seat is AB22.\n<hr/>";
        $this->text .= "4. Congrats, you reached your final destination.";
    }

    public function testOutputFormatedText()
    {
        $formatter = new TextFormatter();
        $formatter->setSorter(new BoardingCardSorter());
        $text = $formatter->format($this->trip);
        $this->assertEquals($this->text, $text);
    }

    public function testExceptionIfSorterNotProvided()
    {
        $formatter = new TextFormatter();

//        $this->expectException(\InvalidArgumentException::class);
        $formatter->setSorter(new BoardingCardSorter());
        $formatter->format($this->trip);
    }

    public function testExceptionIfTripEmpty()
    {
        $formatter = new TextFormatter();


//        $this->expectException(\InvalidArgumentException::class);
        $formatter->setSorter(new BoardingCardSorter());
        $formatter->format([]);
    }

}
