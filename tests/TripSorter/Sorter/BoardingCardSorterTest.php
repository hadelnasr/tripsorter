<?php

namespace TripSorter\Sorter;

use TripSorter\BoardingCard\GenericBoardingCard;

class BoardingCardSorterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GenericBoardingCard
     */
    protected $boardingCardOrigin;

    /**
     * @var GenericBoardingCard
     */
    protected $boardingCardStopover;

    /**
     * @var GenericBoardingCard
     */
    protected $boardingCardDestination;

    public function setUp()
    {
        $this->boardingCardOrigin = new GenericBoardingCard();
        $this->boardingCardOrigin->setDestination('Oslo')
                ->setMean('A380')
                ->setOrigin('Lubumbashi')
                ->setOtherInformation("You may want to get covered before landing. It's freezing")
                ->setSeat('B12')
                ->setTransport('Plane');

        $this->boardingCardStopover = new GenericBoardingCard();
        $this->boardingCardStopover->setTransport('Boat')
                ->setSeat(null)
                ->setOtherInformation("Don't forget your security vest")
                ->setOrigin('Oslo')
                ->setMean("The ebony")
                ->setDestination("Copenhagen");

        $this->boardingCardDestination = new GenericBoardingCard();
        $this->boardingCardDestination->setDestination('Athens')
                ->setMean('BG980I3')
                ->setOrigin('Copenhagen')
                ->setOtherInformation(null)
                ->setSeat('Pres1')
                ->setTransport('Bus');
    }

    public function testOrdersTheBoardingCard()
    {
        $trip = [
            $this->boardingCardDestination,
            $this->boardingCardOrigin,
            $this->boardingCardStopover
        ];

        $sorter = new BoardingCardSorter();

        $orderedTrip = $sorter->sort($trip);

        $this->assertCount(3, $orderedTrip);
        $this->assertTrue($orderedTrip instanceof \Iterator);

        $this->assertEquals($this->boardingCardOrigin, $orderedTrip[0]);
        $this->assertEquals($this->boardingCardStopover, $orderedTrip[1]);
        $this->assertEquals($this->boardingCardDestination, $orderedTrip[2]);

        shuffle($trip);

        $orderedTrip = $sorter->sort($trip);

        $this->assertEquals($this->boardingCardOrigin, $orderedTrip[0]);
        $this->assertEquals($this->boardingCardStopover, $orderedTrip[1]);
        $this->assertEquals($this->boardingCardDestination, $orderedTrip[2]);
    }

    public function testThrowsAnExceptionIfNonContinuesTrip()
    {
        $trip = [
            $this->boardingCardDestination,
            $this->boardingCardOrigin
        ];

        $sorter = new BoardingCardSorter();

//        $this->expectException(NonContinuesTripException::class);
        $sorter->sort($trip);
    }

    public function testThrowsAnExceptionIfANonOrderableBoardingCardIsProvided()
    {
        $trip = [
            $this->boardingCardDestination,
            $this->boardingCardOrigin,
            $this->boardingCardStopover,
            ''
        ];

        $sorter = new BoardingCardSorter();

//        $this->expectException(\InvalidArgumentException::class);
        $sorter->sort($trip);
    }

}
